<?php
require_once './vendor/autoload.php';
include 'Sitemap.php';
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

function getCities($url, $headers){
    $request = Requests::get($url, $headers);
    return json_decode($request->body);
}

function getActivities($url, $headers){
    $request = Requests::get($url, $headers);
    $response = json_decode($request->body);
    return $response->data;
}

function createSitemap(){
    $locales = ['es-ES', 'fr-FR', 'it-IT'];
    $result = [];
    $url =''; 
    
    foreach ($locales as $locale){
        $nameF ='sitemap_%s';
        $sitemap = new Sitemap('');
        $name = sprintf($nameF, $locale);
        $sitemap->setFilename($name);
        $headers = ['Accept-Language' => $locale ];
        $cities = getCities('https://api.musement.com/api/v3/cities?limit=20', $headers);
        foreach($cities as $city){
            $sitemap->addItem($city->url, '0.7');
            $format = 'https://api.musement.com/api/v3/cities/%1$d/activities?limit=20';
            $url= sprintf($format,$city->id);
            $activities = getActivities($url, $headers);
            //echo(json_encode($activities));
            foreach($activities as $activity){
                if (property_exists($activity,'url')){
                    $sitemap->addItem($activity->url, '0.5');
                }
            }
        }
        $localeSlug = explode('-',$locale);
        $formatSitemap = 'https://musement.com/';
        $urlSitemap = sprintf($formatSitemap, $localeSlug[0]);
        $sitemap->createSitemapIndex('https://musement.com/', 'Today');
        echo "Insert receipents for the current locale comma separated: ";
        $handle = fopen ("php://stdin","r");
        $recipients = fgets($handle);
        $subjectFormat = 'MUSEMENT.COM sitemap for %s';
        $subject = sprintf($subjectFormat, $locale);
        sendMail($recipients, $name, $subject);
    }
}

function sendMail($recipients, $attachment, $subject){
    $mail = new PHPMailer;
    $mail->From = "giuseppe.taccardi@gmail.com";
    $mail->FromName = "Giuseppe Taccardi";
    $recipientsList = explode(',',$recipients);
    
    foreach($recipientsList as $recipient){
        $mail->addAddress(trim($recipient));
    }
    //Provide file path and name of the attachments
    $mail->addAttachment($attachment);
    $mail->Subject = $subject;
    $mail->Body = "Here we go!";
    $mail->IsSMTP();
    $mail->Host = "192.168.1.77";
    if(!$mail->send()) {
        echo "Mailer Error: " . $mail->ErrorInfo;
    } 
    else {
        echo "Message has been sent successfully";
    }
}

createSitemap();
?>